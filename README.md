# Demo Pichincha



## Proyecto Demo Banco Pichincha

Proyecto Demo para Banco Pichincha 

### Este proyecto tiene como objetivo mostrar la funcionalidad de Sprig Boot
### Utilitarios previos
```
# Con JDK 11 minimo
# Base de datos Postgres V 11 minimo
# Intellij
# Utiliza Gitlab como repositorio 
# Postman
```
## Pasos
### 1. Creacion credenciales Base de Datos
Este demo utiliza Postgres V 11
```
1. Creamos usuario pichincha con clave pichincha y con privilegios creacion DB
2. Creamos base de datos pichincha y atamos al nuevo usuario
```

### 2. Clonamos el repositorio en el directorio a su eleccion con el comando
```
cd directorioproyecto
Abrimos gitbash
git init
git clone https://gitlab.com/victorjcardenast/demo-pichincha.git
```
### 3. Ejecutamos Intellij y abrimos el proyecto
Esperamos a que MAVEN actualice el proyecto

Ejecutamos el archivo: PichinchaDemoApplication.java
### 4.Inser tabla Utils
En la base de datos ejecutamos
```
INSERT INTO public.utils("name", value) VALUES('limite diario de retiro', '1000');
```
## Api Rest
### Clientes
```
Estados Genero: Masculino, Femenino
DNI: maximo 13 caracteres
URL: http://localhost:8080/api/clientes/create
Metodo: POST
Body:
{
    "password": "password",
    "state": true,
    "dni": "1755678543",
    "names": "XXXXX",
    "surnames": "XXXXX",
    "birthday":"1993-01-26T00:00:00.000Z",
    "address": "Direccion 1",
    "phone": "0979256644",
    "gender": "Masculino"
}
```

### Cuentas
```
accountType: Ahorros, Corriente
URL: http://localhost:8080/api/cuentas/create
Metodo: POST
Body:
{
    "accountNumber": "1234563",
    "accountType": "Corriente",
    "initialBalance": 1000.00,
    "state": true,
    "clients": [
        {"id": 1}
    ]
}
```

### Movimientos
```
transactionType: Retiro, Deposito
URL: http://localhost:8080/api/movimientos/create
Metodo: POST
Body:
{
    "transactionType": "Retiro",
    "accountNumber": "1234564",
    "amount": 10.00
}
```