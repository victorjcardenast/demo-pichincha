package com.bancopichincha.pichinchademo.controllers;

import com.bancopichincha.pichinchademo.entities.dto.ErrorDto;
import com.bancopichincha.pichinchademo.entities.dto.TransactionDto;
import com.bancopichincha.pichinchademo.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/movimientos",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class MovimientosController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody TransactionDto transaction) {
        ErrorDto error = transactionService.save(transaction);

        if (!error.getCode().equals("403")) {
            return ResponseEntity.status(HttpStatus.CREATED).body(error);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
        }
    }
}
