package com.bancopichincha.pichinchademo.controllers;

import com.bancopichincha.pichinchademo.entities.dto.ErrorDto;
import com.bancopichincha.pichinchademo.entities.Client;
import com.bancopichincha.pichinchademo.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/clientes",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientesController {

    @Autowired
    private ClientService clientService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Client client) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(clientService.save(client));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key dni already exists",
                            "SQLState: 23505", "failed"));
        }
    }
}
