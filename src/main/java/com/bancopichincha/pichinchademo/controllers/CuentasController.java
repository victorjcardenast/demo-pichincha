package com.bancopichincha.pichinchademo.controllers;

import com.bancopichincha.pichinchademo.entities.dto.ErrorDto;
import com.bancopichincha.pichinchademo.entities.Account;
import com.bancopichincha.pichinchademo.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/cuentas",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class CuentasController {

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Account account) {

        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(accountService.save(account));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }


    }
}
