package com.bancopichincha.pichinchademo.enums;

public enum AccountType {
    Ahorros,
    Corriente
}
