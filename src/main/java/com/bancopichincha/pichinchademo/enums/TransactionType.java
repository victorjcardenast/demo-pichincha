package com.bancopichincha.pichinchademo.enums;

public enum TransactionType {
    Retiro,
    Deposito
}
