package com.bancopichincha.pichinchademo.entities;

import com.bancopichincha.pichinchademo.enums.Gender;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "clients")
public class Client  extends Person {

    @Column(length = 20, nullable = false)
    private String password;

    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT TRUE")
    private Boolean state;

    @ManyToMany(mappedBy = "clients")
    @JsonIgnoreProperties(value = "clients", allowSetters = true)
    private Set<Account> accounts  = new HashSet<>();

    public Client() {

    }


}
