package com.bancopichincha.pichinchademo.entities;

import com.bancopichincha.pichinchademo.enums.Gender;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 13, nullable = false, unique = true)
    private String dni;

    @Column(length = 50, nullable = false)
    private String names;

    @Column(length = 50, nullable = false)
    private String surnames;

    @Column(nullable = false)
    private Date birthday;

    @Column(length = 100, nullable = false)
    private String address;

    @Column
    private String phone;

    @Enumerated(EnumType.STRING)
    private Gender gender;



}
