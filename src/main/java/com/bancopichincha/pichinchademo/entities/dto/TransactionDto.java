package com.bancopichincha.pichinchademo.entities.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionDto {

    private String transactionType;

    private Double balance;

    private Double amount;

    private String accountNumber;

    public TransactionDto() {
    }


}
