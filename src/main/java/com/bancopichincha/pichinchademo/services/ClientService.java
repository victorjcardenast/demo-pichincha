package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Client;

import java.util.Optional;

public interface ClientService {

    public Iterable<Client> findAll();

    public Optional<Client> findById(Long id);

    public Client save(Client client);

    public void deleteById(Long id);
}
