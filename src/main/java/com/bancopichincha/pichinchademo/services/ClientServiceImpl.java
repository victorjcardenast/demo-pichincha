package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Client;
import com.bancopichincha.pichinchademo.entities.Person;
import com.bancopichincha.pichinchademo.repositories.ClientRepository;
import com.bancopichincha.pichinchademo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Iterable<Client> findAll() {
        return null;
    }

    @Override
    public Optional<Client> findById(Long id) {
        return Optional.empty();
    }

    @Override
    @Transactional
    public Client save(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public void deleteById(Long id) {

    }
}
