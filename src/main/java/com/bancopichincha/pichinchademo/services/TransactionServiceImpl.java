package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Account;
import com.bancopichincha.pichinchademo.entities.Transaction;
import com.bancopichincha.pichinchademo.entities.dto.ErrorDto;
import com.bancopichincha.pichinchademo.entities.dto.TransactionDto;
import com.bancopichincha.pichinchademo.enums.TransactionType;
import com.bancopichincha.pichinchademo.repositories.AccountRepository;
import com.bancopichincha.pichinchademo.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Iterable<Transaction> findAll() {
        return null;
    }

    @Override
    public Optional<Transaction> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public ErrorDto save(TransactionDto transaction) {
//        this.accountStatus(transaction.getAccountNumber(), new Date(), new Date());

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
        String das = sdf.format(new Date());
        Transaction transactionDB = new Transaction();
        Account account = accountRepository.findByAccountNumber(transaction.getAccountNumber()).get();
        Double balance = transactionRepository.lastBalance(account.getAccountNumber());

        ErrorDto error = new ErrorDto();
        switch (transaction.getTransactionType().toLowerCase()) {
            case "retiro":
                if (transactionRepository.validateInitialAmount(account.getAccountNumber())) {
                    if (balance >= transaction.getAmount()) {
                        transactionDB = new Transaction(
                                TransactionType.Retiro
                                , balance - transaction.getAmount()
                                , transaction.getAmount()
                                , account
                        );
                        transactionDB = transactionRepository.save(transactionDB);

                        error = new ErrorDto(
                                "Transaccion realizada con exito" + transactionDB.getId()
                                , "200"
                                , "ok"
                        );
                    } else {
                        error = new ErrorDto(
                                "Monto excede disponible"
                                , "403"
                                , "failed"
                        );
                    }
                }
                if (transactionRepository.validateDailyAmount(account.getAccountNumber()) &&
                        !transactionRepository.validateInitialAmount(account.getAccountNumber())) {
                    if (balance >= transaction.getAmount()) {
                        transactionDB = new Transaction(
                                TransactionType.Retiro
                                , balance - transaction.getAmount()
                                , transaction.getAmount()
                                , account
                        );

                        transactionDB = transactionRepository.save(transactionDB);
                        error = new ErrorDto(
                                "Transaccion realizada con exito" + transactionDB.getId()
                                , "200"
                                , "ok"
                        );
                    } else {
                        error = new ErrorDto(
                                "Monto excede disponible"
                                , "403"
                                , "failed"
                        );
                    }
                } else {
                    error = new ErrorDto(
                            "Cupo diario excedido"
                            , "403"
                            , "failed"
                    );

                }

                break;
            case "deposito":
                transactionDB = new Transaction(
                        TransactionType.Deposito
                        , balance + transaction.getAmount()
                        , transaction.getAmount()
                        , account
                );

                transactionDB = transactionRepository.save(transactionDB);
                error = new ErrorDto(
                        "Transaccion realizada con exito: " + transactionDB.getId()
                        , "200"
                        , "ok"
                );
                break;
            default:
                error = new ErrorDto(
                        "Tipo transaccion no encontrada"
                        , "403"
                        , "failed"
                );
                break;
        }

        return error;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public List<Transaction> accountStatus(String  accountNumber, Date initDate, Date endDate) {
        System.out.println();
        List<Object[]> transactions = new ArrayList<>();

        try {
            transactions = transactionRepository.accountStatus(
//                    accountNumber,
                     new SimpleDateFormat("dd/MM/yyyy").parse("29/07/2022")
                    , new SimpleDateFormat("dd/MM/yyyy").parse("30/07/2022")
            );
            System.out.println();
        } catch (ParseException e) {
            System.out.println();
        }

        return null;
    }
}
