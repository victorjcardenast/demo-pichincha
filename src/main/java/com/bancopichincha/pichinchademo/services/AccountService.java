package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Account;

import java.util.Optional;

public interface AccountService {

    public Iterable<Account> findAll();

    public Optional<Account> findById(Long id);

    public Account save(Account account);

    public void deleteById(Long id);
}
