package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Account;
import com.bancopichincha.pichinchademo.entities.Client;
import com.bancopichincha.pichinchademo.entities.Transaction;
import com.bancopichincha.pichinchademo.enums.TransactionType;
import com.bancopichincha.pichinchademo.repositories.AccountRepository;
import com.bancopichincha.pichinchademo.repositories.ClientRepository;
import com.bancopichincha.pichinchademo.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Iterable<Account> findAll() {
        return null;
    }

    @Override
    public Optional<Account> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Account save(Account account) {
        Set<Client> clients = account.getClients();
        Account accountDb = account;
        accountDb.setClients(new HashSet<>());

        for (Client client : clients) {
            Client clientFind = clientRepository.findById(client.getId()).get();
            accountDb.addClient(clientFind);
        }
        accountDb = accountRepository.save(accountDb);

        Transaction transaction = new Transaction(
                TransactionType.Deposito
                , accountDb.getInitialBalance()
                , accountDb.getInitialBalance()
                , accountDb
                , true
        );
        transaction = transactionRepository.save(transaction);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        accountDb.setTransactions(transactions);
        return accountDb;
    }

    @Override
    public void deleteById(Long id) {

    }
}
