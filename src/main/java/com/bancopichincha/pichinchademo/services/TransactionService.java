package com.bancopichincha.pichinchademo.services;

import com.bancopichincha.pichinchademo.entities.Client;
import com.bancopichincha.pichinchademo.entities.Transaction;
import com.bancopichincha.pichinchademo.entities.dto.ErrorDto;
import com.bancopichincha.pichinchademo.entities.dto.TransactionDto;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionService {

    public Iterable<Transaction> findAll();

    public Optional<Transaction> findById(Long id);

    public ErrorDto save(TransactionDto transaction);

    public void deleteById(Long id);

    public List<Transaction> accountStatus(String  accountNumber, Date initDate, Date endDate) throws ParseException;
}
