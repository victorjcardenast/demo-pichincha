package com.bancopichincha.pichinchademo.repositories;

import com.bancopichincha.pichinchademo.entities.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByAccountNumber(String aLong);
}
