package com.bancopichincha.pichinchademo.repositories;

import com.bancopichincha.pichinchademo.entities.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
}
