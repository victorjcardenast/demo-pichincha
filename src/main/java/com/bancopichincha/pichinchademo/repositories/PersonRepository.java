package com.bancopichincha.pichinchademo.repositories;

import com.bancopichincha.pichinchademo.entities.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
}
