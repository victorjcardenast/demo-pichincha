package com.bancopichincha.pichinchademo.repositories;

import com.bancopichincha.pichinchademo.entities.Util;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilRepository extends CrudRepository<Util, Long> {
}
